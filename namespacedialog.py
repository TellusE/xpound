import os
from tkinter import *
from tkinter.ttk import *

from keyvaldialog import KeyValDialog

class NamespaceDialog(Toplevel):
    def __init__(self, parent, localns, globalns):
        super().__init__(parent)
        self.minsize(400, 300)

        self.title("Namespaces")

        self.unchecked_img = PhotoImage(name="unchecked_box", file=os.path.join(os.path.dirname(os.path.realpath(__file__)), "img", "uncheckbox.gif"))
        self.checked_img = PhotoImage(name="checked_box", file=os.path.join(os.path.dirname(os.path.realpath(__file__)), "img", "checkedbox.gif"))

        self.nsTreeview = Treeview(self, columns=("Key", "Namespace"))
        self.nsTreeview.heading("#0", text="Global")
        self.nsTreeview.column("#0", width=50, anchor=CENTER)
        self.nsTreeview.heading("Key", text="Key")
        self.nsTreeview.column("Key", width=50, stretch=YES)
        self.nsTreeview.heading("Namespace", text="Namespace")
        self.nsTreeview.column("Namespace", width=300, stretch=YES)

        self.nsTreeview.bind("<Button-1>", lambda event: self.treeview_click(event))

        self.localns = localns
        self.globalns = globalns

        for key,val in localns.items():
            self.nsTreeview.insert("", END, image="unchecked_box", values=[key, val])
        for key,val in globalns.items():
            self.nsTreeview.insert("", END, image="checked_box", values=[key, val])

        self.nsTreeview.pack(fill=BOTH, expand=True)

        self.btnFrame = Frame(self)

        self.addbtn = Button(self.btnFrame, text="+", command=self.addEntry)
        self.rembtn = Button(self.btnFrame, text="-", command=self.removeEntry)

        self.addbtn.pack(side=RIGHT)
        self.rembtn.pack(side=RIGHT)

        self.btnFrame.pack(fill=X)

        when_destroy = lambda event: self.destroy()
        self.bind("Alt-O", when_destroy)
        self.bind("<Escape>", when_destroy)
        Button(self, text="OK", underline=0, command=self.destroy).pack(fill=X, expand=True)

    def addEntry(self):
        kv = StringVar()
        vv = StringVar()
        d = KeyValDialog(self, kv, vv)
        self.wait_window(d)

        print("Came back with %s:%s" % (kv.get(), vv.get()))

        if len(kv.get()) > 0 and len(vv.get()) > 0:
            self.nsTreeview.insert("", END, image=self.unchecked_img, values=[kv.get(), vv.get()])
            self.localns[kv.get()] = vv.get()

    def removeEntry(self):
        sel = self.nsTreeview.selection()[0]
        item = self.nsTreeview.item(sel)
        self.nsTreeview.delete(sel)
        if item["image"] == "checked_box":
            self.globalns.pop(item["values"][0])
        else:
            self.localns.pop(item["values"][0])

    def treeview_click(self, event):
        row = self.nsTreeview.identify_row(event.y)
        col = self.nsTreeview.identify_column(event.x)
        if col == "#0" and row != "":
            item = self.nsTreeview.item(row)

            islocal = True if item["image"][0] == "unchecked_box" else False
            key,val = item["values"]

            new_img = ""
            if islocal:
                # Local namespace. Make global.
                new_img = "checked_box"
                self.globalns[key] = val
                self.localns.pop(key)
            else:
                new_img = "unchecked_box"
                self.globalns.pop(key)
                self.localns[key] = val

            self.nsTreeview.item(row, image=new_img)

            print(item)
