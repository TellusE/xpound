# XPound #

XPound is an XPath visualiser written in Python3 with TKinter and lxml.

## Requirements ##

* Python3 (possibly Python 2.7 and up will also work)
* Tkinter with ttk
* lxml
* yaml

### Ubuntu ###

```
sudo apt-get update && sudo apt-get install python3-yaml python3-lxml python3-tk
```

## Usage ##

Simply run the program as you would any other Python script. Optionally, add a file name to open it as the first thing the program opens.

## Contributions ##

I welcome any suggestions or patches, but keep in mind that I mostly expand the project as more/tweaked features are needed in my daily work.