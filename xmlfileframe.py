
import lxml, lxml.etree, time

import tkinter as tk
import tkinter.ttk as ttk

from keyvaldialog import KeyValDialog
from namespacedialog import NamespaceDialog

class XmlFileFrame(ttk.Frame):

    NSFILTER = "nsfilter"
    NSFILTER_FULL = "Full"
    NSFILTER_ALIAS = "Alias"
    NSFILTER_NONE = "None"
    
    def __init__(self, root, notebook, xmlFilePath):
        #Frame.__init__(self, notebook)
        super().__init__(master=notebook)
        self.root = root
        self.parent = notebook
        self.xmlFilePath = xmlFilePath

        self.xmlFile = open(xmlFilePath, "r")
        try:
            self.etree = lxml.etree.parse(self.xmlFile)
        except lxml.etree.Error as e:
            messagebox.showerror("XML Error", "Failed to open %s:\n%s" % (os.path.basename(self.xmlFilePath), e))
        finally:
            self.xmlFile.close()

        # Local namespaces
        self.namespaces = {}

        self.add_query()
        self.add_results()

    def add_query(self):
        xpathFrame = ttk.Frame(self)

        queryAndNS = ttk.Frame(xpathFrame)
        
        self.xpathQuery = ttk.Combobox(queryAndNS)
        self.xpathQuery.bind("<Return>", self.query_button_click)
        
        self.goButton = ttk.Button(queryAndNS, text="Go!", command=self.query_button_click, underline=0, width=3)
        namespaceButton = ttk.Button(queryAndNS, text="NS...", command=self.namespace_button_click, underline=0, width=5)

        self.bind("<Alt-g>", self.query_button_click)
        self.bind("<Alt-n>", self.namespace_button_click)
        self.bind("<Alt-a>", lambda e: self.autoQueryVar.set(False) if self.autoQueryVar.get() else self.autoQueryVar.set(True))

        self.xpathQuery.pack(side=tk.LEFT, fill=tk.X, expand=True)
        self.goButton.pack(side=tk.LEFT)
        namespaceButton.pack(side=tk.LEFT)

        queryAndNS.pack(fill=tk.X, expand=True)

        self.xpathMessages = ttk.Label(xpathFrame, text="XPath notifications go here...")

        self.xpathMessages.pack(side=tk.LEFT)

        xpathFrame.pack(anchor=tk.N, fill=tk.X)
        
    def add_results(self):

        frame = ttk.Frame(self)

        ## Results frame.
        rscroll = ttk.Scrollbar(frame, orient=tk.VERTICAL, takefocus=False)
        self.resultsList = ttk.Treeview(frame, yscrollcommand=rscroll.set)
        rscroll.config(command=self.resultsList.yview)

        self.resultsList.pack(fill=tk.BOTH, expand=True, side=tk.LEFT)
        rscroll.pack(fill=tk.Y, side=tk.LEFT)

        frame.pack(fill=tk.BOTH, expand=True)

        frame = ttk.Frame(self)

        self._result_options = {}
        nsfilter = tk.StringVar(self, value=self.NSFILTER_FULL, name=self.NSFILTER)
        self.result_option(self.NSFILTER, nsfilter)

        ttk.Label(frame, text="Namespace filter: ").pack(side=tk.LEFT)
        menu = ttk.OptionMenu(frame, nsfilter, self.NSFILTER_FULL, self.NSFILTER_FULL, self.NSFILTER_ALIAS, self.NSFILTER_NONE, command=self.query_button_click)
        menu.pack(side=tk.LEFT)

        frame.pack(fill=tk.X, expand=False, anchor=tk.N)

    def result_option(self, key, val=None):
        """Gets or sets a result option. If val is None, this method *gets*,
        otherwise, it sets."""
        if val is not None:
            self._result_options[key]=val
        else:
            return self._result_options.get(key)

    def message(self, msg, color="black"):
        self.xpathMessages.config(text=msg, foreground=color)

    def linkify_msg(self, command):
        """Adds a binding to the message label and changes the cursor when over it."""
        self.linkify_opts = {}
        self.linkify_opts["oldfg"] = self.xpathMessages.cget("foreground")
        self.linkify_opts["oldcursor"] = self.xpathMessages.cget("cursor")
        self.xpathMessages.bind("<Enter>", lambda *x: self.xpathMessages.config(foreground="blue", cursor="hand1"))
        self.xpathMessages.bind("<Leave>", lambda *x: self.xpathMessages.config(foreground=self.linkify_opts["oldfg"], cursor=self.linkify_opts["oldcursor"]))
        self.xpathMessages.bind("<Button-1>", command)

    def delinkify_msg(self):
        """Removes binding and link look on message label."""
        self.xpathMessages.unbind("<Enter>")
        self.xpathMessages.unbind("<Leave>")

    def query_button_click(self, event=None):
        query = self.xpathQuery.get()
        if query not in self.xpathQuery["values"]:
            self.xpathQuery["values"] = tuple(self.xpathQuery["values"]) + (query,)
        self.run_query(query)

    def run_query(self, query):
        print("Attempting to run query %s" % query)
        # Reset message status.
        self.delinkify_msg()
        try:
            start = time.clock()
            ns = self.root.namespaces.copy()
            ns.update(self.namespaces)
            result = self.etree.xpath(query, namespaces=ns)
            if len(self.resultsList.get_children()):
                self.resultsList.delete(*self.resultsList.get_children())
            for r in result:
                self.recursive_insert("", r)
            end = time.clock()
            self.message("Query succeeded (%ss)" % round(end - start, 4), "black")
        except lxml.etree.XPathError as e:
            self.xpath_err = e
            self.message("Error: %s" % e, "red")
            self.linkify_msg(self.popup_xpath_err)

    def popup_xpath_err(self, event=None):
        messagebox.showerror(type(self.xpath_err), self.xpath_err)

    def recursive_insert(self, parent, content):
        tv = self.resultsList

        # Format the tag.
        tag = content.tag
        nsfilter = self.result_option(self.NSFILTER).get()
        if nsfilter != self.NSFILTER_FULL:
            # Strip first {} pattern from tag.
            if tag[0] == "{":
                # Only replace if there seems to be an actual tag.
                if nsfilter == self.NSFILTER_NONE:
                    tag = tag[tag.index("}")+1:].strip()
                elif nsfilter == self.NSFILTER_ALIAS:
                    ns = tag[:tag.index("}")+1]
                    tag = tag.replace(ns, "%s: " % self.get_nskey(ns[1:-1]))

        # Format the text.
        text = ": %s" % content.text if content.text is not None else ""

        parent = tv.insert(parent, tk.END, text="%s%s" % (tag, text), values=[content.attrib, content.values()])
        for key,val in content.attrib.items():
            tv.insert(parent, tk.END, text="%s: %s" % (key, val))

        for child in content:
            self.recursive_insert(parent, child)

    def namespace_button_click(self, event=None):
        d = NamespaceDialog(self, self.namespaces, self.root.namespaces)
        self.wait_window(d)
        #print("Window returned with local %s and global %s" % (self.namespaces, self.root.namespaces))

    def get_nskey(self, value):
        """Retrieves the alias for a namespace. It's a reverse dict search."""

        # Local-first
        d = self.namespaces if value in self.namespaces.values() else None

        # Then global.
        if d is None:
            d = self.root.namespaces if value in self.root.namespaces.values() else None

        # Ditch if neither
        if d is None:
            return None

        for key,val in d.items():
            if val==value:
                return key

        print("ERROR! We should NOT have ended here in execution!")
        return None

    def on_closed(self):
        pass
