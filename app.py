
# Global imports.
import lxml.etree, os, yaml, time
# Local imports.
from xmlfileframe import XmlFileFrame

import tkinter as tk
import tkinter.ttk as ttk

import tkinter.filedialog as filedialog
import tkinter.messagebox as messagebox

def configpath(filename = "config.yaml"):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), filename)

class App(object):
    def __init__(self, root):
        self.root = root

        self.root.minsize(600, 400)

        self.root.title("xPound XPath visualizer")

        # Set defaults:

        # Global namespaces.
        self.namespaces = {}
        self.recent_files = []
        self.open_files = []
        
        # Overwrite with config, if found.
        self.load_config()

        # Globally bind Control+A to select all.
        self.root.bind_class("TCombobox","<Control-a>", lambda event: event.widget.select_range(0, tk.END))

        self.add_menu()
        self.add_notebook()

        self.update_recentfile_menu()

        self.root.protocol("WM_DELETE_WINDOW", self.on_destroy)

    # TODO: refactor btn_press and btn_release into class or module with style code.
    def btn_press(self, event):
        x, y, widget = event.x, event.y, event.widget
        elem = widget.identify(x, y)
        if elem == "": # Assume none clicked.
            return
        index = widget.index("@%d,%d" % (x, y))

        if "close" in elem:
            widget.state(['pressed'])
            widget.pressed_index = index

    def btn_release(self, event):
        x, y, widget = event.x, event.y, event.widget

        if not widget.instate(['pressed']):
            return

        elem =  widget.identify(x, y)
        if elem == "": # Assume none clicked.
            return
        index = widget.index("@%d,%d" % (x, y))

        if "close" in elem and widget.pressed_index == index:
            # Remove tab from notebook.
            widget.forget(index)
            # Pop tab entry in list.
            # TODO / MAJOR PITFALL: If the indices of open_files and self.tabs don't match up, we're in deep doodoo.
            closed_file = self.open_files.pop(index)
            closed_file.on_closed()
            widget.event_generate("<<NotebookClosedTab>>")

        widget.state(["!pressed"])
        widget.pressed_index = None

    def tabs_middlemouse_click(self, event):
        x, y, widget = event.x, event.y, event.widget

        elem = widget.identify(x, y)

        if elem == "":
            return # No element selected.

        index = widget.index("@%d,%d" % (x, y))

        widget.forget(index)

    def add_notebook(self):
        imgdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'img')
        i1 = tk.PhotoImage("img_close", file=os.path.join(imgdir, 'close.gif'))
        i2 = tk.PhotoImage("img_closeactive",
                                file=os.path.join(imgdir, 'close_active.gif'))
        i3 = tk.PhotoImage("img_closepressed",
                                file=os.path.join(imgdir, 'close_pressed.gif'))

        style = ttk.Style()

        style.element_create("close", "image", "img_close",
            ("active", "pressed", "!disabled", "img_closepressed"),
            ("active", "!disabled", "img_closeactive"), border=8, sticky='e')

        style.layout("ButtonNotebook", [("ButtonNotebook.client", {"sticky": "nswe"})])
        style.layout("ButtonNotebook.Tab", [
            ("ButtonNotebook.tab", {"sticky": "nswe", "children":
                [("ButtonNotebook.padding", {"side": "top", "sticky": "nswe",
                                             "children":
                    [("ButtonNotebook.focus", {"side": "top", "sticky": "nswe",
                                               "children":
                        [("ButtonNotebook.label", {"side": "left", "sticky": ''}),
                         ("ButtonNotebook.close", {"side": "left", "sticky": 'e'})]
                    })]
                })]
            })]
        )

        self.root.bind_class("TNotebook", "<ButtonPress-1>", lambda event: self.btn_press(event), True)
        self.root.bind_class("TNotebook", "<ButtonRelease-1>", lambda event: self.btn_release(event))
        self.root.bind_class("TNotebook", "<ButtonRelease-2>", lambda event: self.tabs_middlemouse_click(event))
        self.root.bind("<Control-w>", lambda event: self.close_current())

        self.tabs = ttk.Notebook(width=200, height=200, style = "ButtonNotebook")
        self.tabs.pressed_index = None
        self.tabs.pack(fill=tk.BOTH, expand=True)

        # Example usage of tab close event. Can't really use it much, looks like mouse click data.
        #self.tabs.bind("<<NotebookClosedTab>>", lambda event: print("Tab closed! %s" % event.__dict__.keys()))
        self.tabs.enable_traversal()

    def close_current(self):
        """Closes the currently open tab, if one exists."""
        to_close = self.tabs.select()
        if len(to_close) > 0:
            self.tabs.forget(self.tabs.index(to_close))

    def load_config(self):
        """Attempts to load up the configuration."""
        print("Loading config from %s" % configpath())
        try:
            self.config = yaml.load(open(configpath(), "r"))
            self.namespaces = self.config["namespaces"]
            self.recent_files = self.config["recentfiles"]
        except FileNotFoundError as e:
            print("Could not load config (ignoring): %s" % e)

    def add_menu(self):
        ## Menu
        menubar = tk.Menu(self.root)
        menubar.add_command(label="Open...", command=self.open_button_click)

        self.recent_menu = tk.Menu(menubar)
        menubar.add_cascade(label="Recent files", underline=0, menu=self.recent_menu)

        self.root.config(menu=menubar)

    def update_recentfile_menu(self):
        self.recent_menu.delete(0, tk.END)
        for item in self.recent_files:
            self.recent_menu.add_command(label=item, command=self._generate_file_open_cmd(item))
        self.recent_menu.add_command(label="Clear recent", command=self.clear_recentfiles)

    def _generate_file_open_cmd(self, path):
        return lambda: self.open_file(path)

    def add_recentfile(self, path):
        if path in self.recent_files:
            # Re-order to place chosen file at the top.
            print("Re-order list: (Top: %s)" % path)
            for l in self.recent_files:
                print(l)
            self.recent_files.remove(path)
            self.recent_files.insert(0, path)
            print("New order: (Top: %s)" % path)
            for l in self.recent_files:
                print(l)
            self.update_recentfile_menu()
        else:
            self.recent_files.append(path)
            self.update_recentfile_menu()
        
    def rem_recentfile(self, path):
        if path not in self.recent_files:
            return # Nothing to remove.
        else:
            self.recent_files.remove(path)
            self.update_recentfile_menu()

    def clear_recentfiles(self):
        self.recent_files.clear()
        self.update_recentfile_menu()

    def run(self):
        self.root.mainloop()

    def open_button_click(self, event=None):
        xmlFilePath = filedialog.askopenfilename(filetypes=[("XML files", "*.xml"), ("all files", ".*")])
        if len(xmlFilePath) == 0:
            print("Assuming cancelled selection")
            return

        self.open_file(xmlFilePath)

    def open_file(self, xmlFilePath):
        # Currently we allow multiple instances of the same file. This would be the place to change that.
        
        xmlFile = open(xmlFilePath, "r")
        etree = None
        try:
            etree = lxml.etree.parse(xmlFile)
            xmlFile.close()
            newTab = XmlFileFrame(self, self.tabs, xmlFilePath)
            self.tabs.add(newTab, text=os.path.basename(xmlFilePath), padding=3)
            self.add_recentfile(xmlFilePath)
            self.open_files.append(newTab)
        except lxml.etree.Error as e:
            messagebox.showerror("XML Error", "Failed to open %s:\n%s" % (os.path.basename(xmlFilePath), e))

    def on_destroy(self):
        print("Writing config to %s" % configpath())
        yaml.dump({ "namespaces": self.namespaces, "recentfiles": self.recent_files }, open(configpath(), "w"), default_flow_style=False)
        self.root.destroy()
