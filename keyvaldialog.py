from tkinter import *
from tkinter.ttk import *

class KeyValDialog(Toplevel):
    def __init__(self, parent, keyVar, valVar):
        super().__init__(parent)
        self.title("Add Namespace")

        self.keyVar = keyVar
        self.valVar = valVar

        inputFrame = Frame(self)

        Label(inputFrame, text="Key:").pack(anchor=W)
        self.keyEntry = Entry(inputFrame)
        self.keyEntry.pack(fill=X, expand=True)

        Label(inputFrame, text="Value:").pack(anchor=W)
        self.valEntry = Entry(inputFrame, width=70)
        self.valEntry.pack(fill=X, expand=True)

        inputFrame.pack(anchor=NW, fill=X, expand=True, pady=5, padx=5)

        btnFrame = Frame(self)

        Button(btnFrame, text="Ok", command=self.onOk).pack(side=LEFT)
        Button(btnFrame, text="Cancel", command=self.destroy).pack(side=LEFT)

        btnFrame.pack(pady=5, padx=5)

        onok_handler = lambda event: self.onOk()
        self.bind("<Return>", onok_handler)
        self.bind("Alt-O", onok_handler)

        self.update()
        self.minsize(self.winfo_width(), self.winfo_height())
        self.resizable(True, False)

    def onOk(self):
        self.keyVar.set(self.keyEntry.get())
        self.valVar.set(self.valEntry.get())
        self.destroy()
