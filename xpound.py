#!/usr/bin/env python3

""" (Very) simple XPath visualizer for Python3. Loads a file via lxml, then
displays results of xpath expressions. """

import sys, os

from app import App

from tkinter import Tk

def start_app(app=None):
    if app is not None:
        app.root.destroy()
    root = Tk()
    app = App(root)
    if len(sys.argv) > 1:
        init_file = sys.argv[-1]
        if os.path.exists(init_file) and os.path.isfile(init_file):
            app.open_file(init_file)
    app.run()

if __name__ == "__main__":
    start_app()
